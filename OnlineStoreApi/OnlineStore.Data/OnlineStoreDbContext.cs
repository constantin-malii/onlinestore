﻿using Microsoft.EntityFrameworkCore;
using OnlineStore.Domain;
using System;

namespace OnlineStore.Data
{
    public class OnlineStoreDbContext : DbContext
    {
        public OnlineStoreDbContext(DbContextOptions<OnlineStoreDbContext> options) : base(options)
        {

        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Order>()
                .HasKey(o => o.Id);

            modelBuilder.Entity<Order>()
                .Property(o => o.Id);

            modelBuilder.Entity<Order>()
                .Property(o => o.Guid)
                .IsRequired();

            modelBuilder.Entity<Order>()
                .Property(o => o.DatePlaced)
                .IsRequired()
                .HasColumnType("Date");

            modelBuilder.Entity<Order>()
                .HasMany(o => o.CartItems);

            modelBuilder.Entity<Product>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<Product>()
                .Property(p => p.Id);

            modelBuilder.Entity<Product>()
                .Property(p => p.Guid)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.Description);

            modelBuilder.Entity<Product>()
                .Property(p => p.ImageFile);

            modelBuilder.Entity<Product>()
                .Property(p => p.Name)
                .HasColumnType("varchar(50)")
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.Price)
                .IsRequired()
                .HasColumnType("decimal(10,2)")
                .HasDefaultValue(0m);


            modelBuilder.Entity<ShoppingCart>()
                .HasKey(sc => sc.Id);

            modelBuilder.Entity<ShoppingCart>()
                .Property(sc => sc.Id);

            modelBuilder.Entity<ShoppingCart>()
                .Property(sc => sc.Guid)
                .IsRequired();

            modelBuilder.Entity<ShoppingCart>()
                .HasMany(sc => sc.CartItems);

            modelBuilder.Entity<ShoppingCart>()
                .Property(sc => sc.DateCreated)
                .IsRequired()
                .HasColumnType("Date");

            modelBuilder.Entity<ShoppingCart>()
                .Property(sc => sc.DateUpdated)
                .IsRequired()
                .HasColumnType("Date");

            modelBuilder.Entity<CartItem>()
                .Property(ci => ci.Guid)
                .IsRequired();

            modelBuilder.Entity<CartItem>()
                .HasKey(ci => ci.Id);

            modelBuilder.Entity<CartItem>()
                .Property(ci => ci.Id);

            modelBuilder.Entity<CartItem>()
                .HasOne(ci => ci.Product);

            modelBuilder.Entity<CartItem>()
                .Property(ci => ci.Quantity)
                .IsRequired();

            modelBuilder.Entity<CartItem>()
                .HasOne<Product>()
                .WithOne()
                .HasForeignKey<CartItem>(p=>p.ProductId);

            modelBuilder.Entity<Product>()
                .HasData(
                new Product() { Id = 1, Guid = Guid.NewGuid(), Name = "Product1", Price = 22.04M },
                new Product() { Id = 2, Guid = Guid.NewGuid(), Name = "Product2", Price = 33.04M },
                new Product() { Id = 3, Guid = Guid.NewGuid(), Name = "Product3", Price = 44.04M });
        }
    }
}
