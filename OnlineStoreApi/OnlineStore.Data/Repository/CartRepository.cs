﻿using OnlineStore.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStore.Data.Repository
{
    public class CartRepository : IRepository<ShoppingCart>
    {
        private readonly OnlineStoreDbContext ctx;

        public CartRepository(OnlineStoreDbContext ctx) => this.ctx = ctx;
        public async Task<IEnumerable<ShoppingCart>> GetAll() => throw new NotImplementedException();
        public async Task<ShoppingCart> GetById(Guid guid) => await ctx.ShoppingCarts.FindAsync(guid);
        public async Task<IEnumerable<ShoppingCart>> GetFiltered(string filter) => throw new NotImplementedException();
        public async Task Save(ShoppingCart item)
        {
            ctx.ShoppingCarts.Add(item);
            await ctx.SaveChangesAsync();
        }
        public Task SaveAll(IList<ShoppingCart> items) => throw new NotImplementedException();
    }
}
