﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStore.Data.Repository
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(Guid guid);
        Task Save(T item);
        Task SaveAll(IList<T> items);
        Task<IEnumerable<T>> GetFiltered(string filter);
    }
}
