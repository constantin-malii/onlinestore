﻿using Microsoft.EntityFrameworkCore;
using OnlineStore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStore.Data.Repository
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly OnlineStoreDbContext ctx;

        public ProductRepository(OnlineStoreDbContext ctx) => this.ctx = ctx;
        public async Task<IEnumerable<Product>> GetAll() => await ctx.Products.ToListAsync();
        public async Task<Product> GetById(Guid guid) => await ctx.Products.Where(x=>x.Guid==guid).FirstOrDefaultAsync();
        public async Task<IEnumerable<Product>> GetFiltered(string filter) => await ctx.Products.Where(x => x.Name.Contains(filter)).ToListAsync();
        public Task Save(Product item) => throw new NotImplementedException();
        public Task SaveAll(IList<Product> items) => throw new NotImplementedException();
    }
}
