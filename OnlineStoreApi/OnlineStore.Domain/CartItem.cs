﻿using System;
using System.Collections.Generic;

namespace OnlineStore.Domain
{
    public class CartItem
    {
        public Guid Guid { get; set; }
        public long? Id { get; set; }
        public long? ProductId { get; set; }
        public virtual Product Product { get; private set; }
        public Int32 Quantity{ get; set; }
    }
}