﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Domain
{
    public class ShoppingCart
    {
        public long? Id { get; set; }
        public Guid Guid { get; set; }
        public virtual IEnumerable<CartItem> CartItems { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
