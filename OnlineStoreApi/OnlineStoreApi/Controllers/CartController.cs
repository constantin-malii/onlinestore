﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Data.Repository;
using OnlineStore.Domain;

namespace OnlineStoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IRepository<ShoppingCart> shopingCartRepo;
        private readonly IRepository<Product> productRepo;

        public CartController(IRepository<ShoppingCart> shopingCartRepo, IRepository<Product> productRepo)
        {
            this.shopingCartRepo = shopingCartRepo;
            this.productRepo = productRepo;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Create(string productId)
        {
            if (Guid.TryParse(productId, out Guid resultGuid))
            {
                // verify if product exists
                var product = productRepo.GetById(resultGuid);
                if (product == null)
                {
                    return NotFound("The product in the cart was not found.");
                }
                var cart = new ShoppingCart
                {
                    CartItems = new List<CartItem> { new CartItem {
                        Guid = Guid.NewGuid(),
                        ProductId = product.Result.Id,
                        Quantity = 1 } },
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow,
                    Guid = Guid.NewGuid()
                };

              await shopingCartRepo.Save(cart);
            }
            return BadRequest($"The provided product id: {productId} is not a valid guid.");
        }
    }
}