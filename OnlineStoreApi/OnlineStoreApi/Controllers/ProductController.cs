﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Data.Repository;
using OnlineStore.Domain;
using OnlineStoreApi.Models;

namespace OnlineStoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IRepository<OnlineStore.Domain.Product> repo;

        public ProductController(IRepository<OnlineStore.Domain.Product> repo)
        {
            this.repo = repo;
        }
        [HttpGet]
        public async Task<IEnumerable<Models.Product>> GetAllProducts(string filter)
        {
                var products = string.IsNullOrEmpty(filter)? 
                                                            await repo.GetAll():
                                                            await repo.GetFiltered(filter);

            return products.Select(x => x.ToWebModel());
        }
    }
}