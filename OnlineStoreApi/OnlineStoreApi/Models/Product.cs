﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStoreApi.Models
{
    public class Product
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }

    public static class ProductExtension
    {
        public static Product ToWebModel(this OnlineStore.Domain.Product obj) => new Product()
        {
            Guid = obj.Guid,
            Name = obj.Name,
            Price = obj.Price
        };

        public static OnlineStore.Domain.Product ToDomainModel(this Product obj) => new OnlineStore.Domain.Product()
        {
            Guid = obj.Guid,
            Name = obj.Name,
            Price = obj.Price
        };
    }
}
