﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineStore.Data;
using OnlineStore.Data.Repository;
using OnlineStore.Domain;

namespace OnlineStoreApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<OnlineStoreDbContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("OnlineStoreConnection")));
            services.AddTransient<IRepository<Product>, ProductRepository>();
            services.AddTransient<IRepository<ShoppingCart>, CartRepository>();

            services.AddCors(options => options.AddPolicy("AllowCors",
            builder =>
            {
                builder
                .WithOrigins("http://localhost:3000")
                .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS")
                .AllowAnyHeader(); 
            }));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            OnlineStoreDbContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("AllowCors");
            app.UseHttpsRedirection();
            app.UseMvc();

           dbContext.Database.EnsureCreated();
        }
    }
}
